import IcBack from './ic-back.svg';
import IcHomeOn from './ic_home.svg';
import IcHomeOff from './ic_home_off.svg';
import IcOrderOn from './ic_order.svg';
import IcOrderOff from './ic_order_off.svg';
import IcProfileOn from './ic_profile.svg';
import IcProfileOff from './ic_profile_off.svg';
import IcStarOn from './ic_star_on.svg';
import IcStarOff from './ic_star_off.svg';
import IcBackWhite from './ic-back-white.svg';
import IcMin from './btn_min.svg';
import IcPlus from './btn_add.svg';
import IcNext from './ic-next.svg';

export {
    IcBack,
    IcHomeOn,
    IcHomeOff,
    IcOrderOn,
    IcOrderOff,
    IcProfileOn,
    IcProfileOff,
    IcStarOn,
    IcStarOff,
    IcBackWhite,
    IcMin,
    IcPlus,
    IcNext,
}