import LogoDawang from './dawang.svg';
import LogoDawangPng from './dawang.png';
import IlSuccessSignUp from './SuccessSignUp.svg';
import IlSuccessOrder from './SuccessOrder.svg';
import IlEmptyOrder from './EmptyOrder.svg';

export {
    LogoDawang,
    LogoDawangPng,
    IlSuccessSignUp,
    IlSuccessOrder,
    IlEmptyOrder
}