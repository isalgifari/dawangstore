import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { IlEmptyOrder } from '../../../assets'
import { Button, Gap } from '../../atoms'
import { useNavigation } from '@react-navigation/native'

const EmptyOrder = () => {
    const navigation = useNavigation()
    return (
        <View style={styles.page}>
            <IlEmptyOrder/>
            <Gap height={30} />
            <Text style={styles.text1}>Ouch! Error</Text>
            <Text style={styles.text2}>Kami tidak menemukan</Text>
            <Text style={styles.text2}>Barang yang kamu mau</Text>
            <Gap height={30} />
            <View style={styles.buttonContainer}>
                <Button
                    text='Cari Barang'
                    onPress = {() => navigation.replace('MainApp')}
                />
            </View>
        </View>
    )
}

export default EmptyOrder

const styles = StyleSheet.create({
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    text1: {
        fontFamily: 'Poppins-Regular',
        fontSize: 20,
        color: '#020202'
    },
    text2: {
        fontFamily: 'Poppins-Light',
        fontSize: 14,
        color: '#8D92A3'
    },
    buttonContainer: {
        width: '100%' ,
        paddingHorizontal: 80
    }
})
