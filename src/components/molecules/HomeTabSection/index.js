import React, {useEffect} from 'react'
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { ItemListFood } from '..';
import { Ip12Pro, Ip12, IpXr, IpX } from '../../../assets';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import { getFoodDataByTypes } from '../../../redux/action';

const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicator}
      style={{ 
        backgroundColor: 'white', 
        elevation: 0, 
        shadowOpacity: 0,
        borderBottomColor: '#F2F2F2',
        borderBottomWidth: 1  
      }}
      tabStyle={{ width: 'auto'}}
      renderLabel={({ route, focused, color }) => (
        <Text style={{ 
            fontFamily: 'Poppins-Medium',
            color: focused ? '#020202' : '#8D92A3' 
        }}>
          {route.title}
        </Text>
      )}
    />
  );

const NewItems = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {newItems} = useSelector((state) => state.homeReducer)

  useEffect(() => {
    dispatch(getFoodDataByTypes('new_items'));
  }, [])
    return (
      <View style={{ paddingTop: 5, paddingHorizontal: 24 }}>
        {newItems.map((item) => {
          return (
            <ItemListFood
              key={item.id}
              rating={item.rate}
              image={{ uri: item.picturePath }} 
              nameProduct={item.name} 
              price={item.price} 
              type="product" 
              onPress={() => navigation.navigate('FoodDetail', item)} 
            />
          )
        })}
      </View>
    )
  };
   
  const Popular = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const {popular} = useSelector((state) => state.homeReducer)
  
    useEffect(() => {
      dispatch(getFoodDataByTypes('popular'));
    }, [])
      return (
        <View style={{ paddingTop: 5, paddingHorizontal: 24 }}>
        {popular.map((item) => {
          return (
            <ItemListFood
              key={item.id}
              rating={item.rate}
              image={{ uri: item.picturePath }} 
              nameProduct={item.name} 
              price={item.price} 
              type="product" 
              onPress={() => navigation.navigate('FoodDetail', item)} 
            />
          )
        })}
      </View>
    )
  };

  const Recommended = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const {recommended} = useSelector((state) => state.homeReducer)
  
    useEffect(() => {
      dispatch(getFoodDataByTypes('recommended'));
    }, [])
      return (
        <View style={{ paddingTop: 5, paddingHorizontal: 24 }}>
        {recommended.map((item) => {
          return (
            <ItemListFood
              key={item.id}
              rating={item.rate}
              image={{ uri: item.picturePath }} 
              nameProduct={item.name} 
              price={item.price} 
              type="product" 
              onPress={() => navigation.navigate('FoodDetail', item)} 
            />
          )
        })}
      </View>
    )
  };
   
  const initialLayout = { width: Dimensions.get('window').width };

const HomeTabSection = () => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'New Items' },
        { key: 'second', title: 'Popular' },
        { key: 'third', title: 'Recommended' },
    ]);
    
    const renderScene = SceneMap({
        first: NewItems,
        second: Popular,
        third: Recommended,
    });
    return (
        <TabView
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            style={{ backgroundColor: 'white' }}
        />
    )
}

export default HomeTabSection

const styles = StyleSheet.create({
    indicator: { 
        backgroundColor: '#020202',
        width: '15%',
        marginLeft: '3%'
    }
})
