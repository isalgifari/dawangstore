import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { ProfileDummy } from '../../../assets'
import { getData } from '../../../utils'
import { useNavigation } from '@react-navigation/native'

const HomeProfile = () => {
    const navigation = useNavigation()
    const [photo, setPhoto] = useState(ProfileDummy)
    
    useEffect(() => {
        navigation.addListener('focus', () => {
            getData('userProfile').then((res) => {
                console.log('user profile: ', res) 
                setPhoto({uri: res.profile_photo_url})
            })
        })
    }, [navigation])
    
    return (
        <View style={styles.profileContainer}>
            <View>
                <Text style={styles.title}>dawangStore</Text>
                <Text style={styles.desc}>Pilih yang kamu mau</Text>
            </View>
            <Image source={photo} style={styles.profile} />
        </View>
    )
}

export default HomeProfile

const styles = StyleSheet.create({
    profileContainer: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    title: {
        fontFamily: 'Poppins-Medium',
        fontSize: 22,
        color: '#020202',
    },
    desc: {
        fontFamily: 'Poppins-Regular',
        fontSize: 14,
        color: '#8D92A3'
    },
    profile: {
        width: 50,
        height: 50
    }
})
