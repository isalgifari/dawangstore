import React from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StyleSheet, Text, View, Dimensions, ScrollView } from 'react-native'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import {useNavigation} from '@react-navigation/native';
import ItemListMenuProfile from '../ItemListMenuProfile';
const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicator}
      style={{ 
        backgroundColor: 'white', 
        elevation: 0, 
        shadowOpacity: 0,
        borderBottomColor: '#F2F2F2',
        borderBottomWidth: 1  
      }}
      tabStyle={{ width: 'auto'}}
      renderLabel={({ route, focused, color }) => (
        <Text style={{ 
            fontFamily: 'Poppins-Medium',
            color: focused ? '#020202' : '#8D92A3' 
        }}>
          {route.title}
        </Text>
      )}
    />
  );

const Account = () => {
  const navigation = useNavigation();
  const SignOut = () => {
    AsyncStorage.multiRemove(['userProfile', 'token'])
      .then(() => {
        navigation.reset({index: 0, routes: [{name: 'SignIn'}]})
      })
  }
    return (
      <View style={{ paddingTop: 5, paddingHorizontal: 24 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
            <ItemListMenuProfile name="Edit Profile" />
            <ItemListMenuProfile name="Home Address" />
            <ItemListMenuProfile name="Security" />
            <ItemListMenuProfile name="Payments" />
            <ItemListMenuProfile name="SignOut" onPress={SignOut} />
        </ScrollView>
      </View>
    )
  };
   
  const dawangStore = () => {
    const navigation = useNavigation();
      return (
        <View style={{ paddingTop: 5, paddingHorizontal: 24 }}>
          <ScrollView showsVerticalScrollIndicator={false}>
              <ItemListMenuProfile name="Rate App" />
              <ItemListMenuProfile name="Help Center" />
              <ItemListMenuProfile name="Privacy & Policy" />
              <ItemListMenuProfile name="Terms & Conditions" />
          </ScrollView>
        </View>
    )
  };

  
   
  const initialLayout = { width: Dimensions.get('window').width };

const ProfileTabSection = () => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Account' },
        { key: 'second', title: 'dawangStore' },
    ]);
    
    const renderScene = SceneMap({
        first: Account,
        second: dawangStore,
    });
    return (
        <TabView
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            style={{ backgroundColor: 'white' }}
        />
    )
}

export default ProfileTabSection

const styles = StyleSheet.create({
    indicator: { 
        backgroundColor: '#020202',
        width: '15%',
        marginLeft: '3%'
    }
})
