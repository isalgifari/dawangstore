import axios from "axios"
import { showMessage, storeData } from "../../utils"
import { setLoading } from "./global"
import { API_HOST } from "../../config"

export const signUpAction = (dataRegister, photoReducer, navigation) => (dispatch) => {
    axios.post(`${API_HOST.url}/api/register`, dataRegister)
            .then((res) => {
                //console.log('data success: ', res.data)
                const profile =  res.data.data.user
                const token = `${res.data.data.token_type} ${res.data.data.access_token}`
                
                storeData('token', {value: token})
                
                if(photoReducer.isUploadPhoto){
                    const photoForUpload = new FormData()
                    photoForUpload.append('file', photoReducer)

                    axios.post(`${API_HOST.url}/api/user/photo`,
                    photoForUpload,
                    {
                        headers: {
                            'Authorization': token,
                            'Content-Type' : 'multipart/form-data',
                       }
                    })
                    .then(resUpload => {
                        profile.profile_photo_url = `${API_HOST.url}/storage/${resUpload.data.data[0]}`
                        storeData('userProfile', profile);
                        navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]})
                    })
                    .catch(err => {
                        showMessage('gagal upload photo coy', err)
                        navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]})
                    }) 
                } else {
                    storeData('userProfile', profile)
                    navigation.reset({index: 0, routes: [{name: 'SuccessSignUp'}]})
                }
                dispatch(setLoading(false))
            }).catch((err) => {
                dispatch(setLoading(false))
                //console.log('sign up error: ', err.response.data.message)
                showMessage(err?.response?.data?.message)
            })
}

export const signIndAction = (form, navigation) => (dispatch) => {
    dispatch(setLoading(true))
    axios.post(`${API_HOST.url}/api/login`, form)
    .then((res) => {
        //console.log('success', res);
        const token = `${res.data.data.token_type} ${res.data.data.access_token}`
        const profile =  res.data.data.user
        dispatch(setLoading(false))
        storeData('token', {value: token})
        storeData('userProfile', profile)
        navigation.reset({index: 0, routes: [{name: 'MainApp'}]})
    }).catch(err => {
        //console.log('error', err.response.data.message)
        dispatch(setLoading(false))
        showMessage(err?.response?.data?.message)
    })
}