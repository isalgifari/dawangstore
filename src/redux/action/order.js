import { getData } from "../../utils"
import axios from "axios"
import { API_HOST } from "../../config"

export const getOrders = () => (dispatch) => {
    getData('token').then(resToken => {
        //console.log('token in progress: ', resToken.value)
        axios.get(`${API_HOST.url}/api/transaction`, {
            headers: {
                'Authorization' : resToken.value
            }
        })
        .then(res => {
            console.log('get orders : ', res.data)
            dispatch({type: 'SET_ORDER', value: res.data.data.data})
        })
        .catch(err => {
            console.log('err: ', err.response)
        })
    })
}

export const getInProgress = () => (dispatch) => {
    getData('token').then(resToken => {
        //console.log('token in progress: ', resToken.value)
        axios.all([
            axios.get(`${API_HOST.url}/api/transaction?status=PENDING`, {
                headers: {
                    'Authorization' : resToken.value
                }
            }),
            axios.get(`${API_HOST.url}/api/transaction?status=SUCCESS`, {
                headers: {
                    'Authorization' : resToken.value
                }
            }),
            axios.get(`${API_HOST.url}/api/transaction?status=ON_DELIVERY`, {
                headers: {
                    'Authorization' : resToken.value
                }
            }),
        ])
        .then(axios.spread((res1, res2, res3) => {
            // console.log('get in progress pending : ', res1.data)
            // console.log('get in progress success : ', res2.data)
            // console.log('get in progress on delivery : ', res3.data) 
            const pending = res1.data.data.data
            const success = res2.data.data.data
            const onDelivery = res3.data.data.data
            dispatch({type: 'SET_IN_PROGRESS', value: [...pending, ...success, ...onDelivery]})
        }))
        .catch(err => {
            console.log('err: ', err.response)
        })
    })
}

export const getPastOrders = () => (dispatch) => {
    getData('token').then(resToken => {
        //console.log('token in progress: ', resToken.value)
        axios.all([
            axios.get(`${API_HOST.url}/api/transaction?status=CANCELLED`, {
                headers: {
                    'Authorization' : resToken.value
                }
            }),
            axios.get(`${API_HOST.url}/api/transaction?status=DELIVERED`, {
                headers: {
                    'Authorization' : resToken.value
                }
            })
        ])
        .then(axios.spread((res1, res2) => {
            // console.log('get past orders canccelled : ', res1.data)
            // console.log('get past orders delivered : ', res2.data)
            const canccelled = res1.data.data.data
            const delivered = res2.data.data.data
            dispatch({type: 'SET_PAST_ORDER', value: [...canccelled, ...delivered]})
        }))
        .catch(err => {
            console.log('err: ', err.response)
        })
    })
}
