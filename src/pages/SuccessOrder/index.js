import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { IlSuccessOrder } from '../../assets'
import { Button, Gap } from '../../components'

const SuccessOrder = ({navigation}) => {
    return (
        <View style={styles.page}>
            <IlSuccessOrder/>
            <Gap height={30} />
            <Text style={styles.text1}>Kamu telah melakukan order</Text>
            <Text style={styles.text2}>Kamu hanya perlu menunggu di rumah</Text>
            <Text style={styles.text2}>kami akan mengemas pesanan kamu</Text>
            <Gap height={30} />
            <View style={styles.buttonContainer}>
                <Button
                    text='Order Lagi'
                    onPress = {() => navigation.replace('MainApp')}
                />
                <Gap height={12} />
                <Button
                    text='Lihat Orderan Saya'
                    color = {'#8D92A3'}
                    onPress = {() => navigation.replace('MainApp', {screen: 'Order'})}
                />
            </View>
        </View>
    )
}

export default SuccessOrder

const styles = StyleSheet.create({
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    text1: {
        fontFamily: 'Poppins-Regular',
        fontSize: 20,
        color: '#020202'
    },
    text2: {
        fontFamily: 'Poppins-Light',
        fontSize: 14,
        color: '#8D92A3'
    },
    buttonContainer: {
        width: '100%' ,
        paddingHorizontal: 80
    }
})
