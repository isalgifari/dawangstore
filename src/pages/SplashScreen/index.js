import React, {useEffect} from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { LogoDawangPng } from '../../assets'
import { getData } from '../../utils'

const SplashScreen = ({navigation}) => {
    useEffect(() => {
      setTimeout(() => {
        getData('token').then(res => {
            //console.log('token: ', res)
            if(res) {
                navigation.reset({index: 0, routes: [{name: 'MainApp'}]});
            }else {
                navigation.replace('SignIn')
            }
        })
      }, 2000)
    }, [])
    return (
        <View style={styles.page}>
            <Image source={LogoDawangPng} style={styles.logo} />
            <Text style={styles.text}>dawangStore</Text>
        </View>
    )
}

export default SplashScreen

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 35,
        color: '#020202',
        fontFamily: 'Poppins-Medium'
    },
    logo: {
        width: 200,
        height: 200
    }
})
