import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { EmptyOrder, Header, OrderTabSection } from '../../components'
import {useDispatch, useSelector} from 'react-redux'
import { getOrders } from '../../redux/action'

const Order = () => {
    const [isEmpty] = useState(false)
    const dispatch = useDispatch();
    const {orders} = useSelector((state) => state.orderReducer)

    useEffect(() => {
        dispatch(getOrders())
    }, [])

    console.log('list orders : ', orders)

    return (
        <View style={styles.page}>
            {orders.length < 1 ? (
            <EmptyOrder />
            ) : (
                <View style={styles.content}>
                    <Header title="Orderan Kamu" />
                    <View style={styles.orderTabSection}>
                        <OrderTabSection />
                    </View>
                </View>
            )}
        </View>
    )
}

export default Order

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    content: {
        flex: 1
    },
    orderTabSection: {
        flex: 1,
        marginTop: 12
    }
})
