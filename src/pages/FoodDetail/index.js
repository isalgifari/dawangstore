import React, {useState, useEffect} from 'react'
import { ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IcBack } from '../../assets'
import { Button, Counter, Gap, Number, Rating } from '../../components'
import { getData } from '../../utils'

const FoodDetail = ({navigation, route}) => {

    const {name, description, price, rate, picturePath, id } = route.params
    const [totalItem, setTotalItem] = useState(1)
    const [userProfile, setUserProfile] = useState({})

    useEffect(() => {
        getData('userProfile').then(res => {
            console.log('profile: ', res)
            setUserProfile(res)
        })
    }, [])

    const onCounterChange = (value) => {
        console.log('counter: ', value)
        setTotalItem(value)
    }

    const onOrder = () => {
        const totalPrice = totalItem * price
        const driver = 50000
        const tax = 10 / 100 * totalPrice
        const total = totalPrice + driver + tax
        const data = {
            item : {
                id: id,
                name: name,
                price: price,
                picturePath: picturePath
            },
            transaction: {
                totalItem: totalItem,
                totalPrice: totalPrice,
                driver: driver,
                tax: tax,
                total: total
            },
            userProfile
        }
        console.log('data for check: ', data)
        navigation.navigate('OrderSummary', data)
    }

    return (
        <View style={styles.page}>
            <ImageBackground  source={{ uri: picturePath }} style={styles.cover} > 
                <TouchableOpacity 
                    style={styles.back} 
                    onPress={() => navigation.goBack()}
                >
                    <IcBack />
                </TouchableOpacity>
            </ImageBackground >
            <View style={styles.content}>
                <View style={styles.mainContent}>
                    <View style={styles.productContainer}>
                        <View>
                            <Text style={styles.productName}>{name}</Text>
                            <Rating number={rate}/>
                        </View>
                        <Counter
                            onValueChange={onCounterChange}
                        />
                    </View>
                    <Gap height={14} />
                    <Text style={styles.desc}>
                        {description}
                    </Text>
                    <Gap height={20} />
                </View>
                <View style={styles.footer}> 
                    <View style={styles.priceContainer}>
                        <Text style={styles.labelTotal}>Total Price:</Text>
                        <Number number={price * totalItem} style={styles.priceTotal} />
                        {/* <Text style={styles.priceTotal}>IDR 20.000.000</Text> */}
                    </View>
                    <View style={styles.button}>
                        <Button 
                            text="Order Now" 
                            color={'white'} 
                            textColor={'black'}
                            onPress= {onOrder}
                        />
                    </View>
                </View>
            </View>
        </View>
    )
}

export default FoodDetail

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white'
    },
    cover: {
        height: 330,
        paddingTop: 24,
        paddingLeft: 16,
        margin: 10,
    },
    back: {
        width: 30, 
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content : {
        backgroundColor: '#6FCF97',
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        paddingVertical: 26,
        paddingHorizontal: 16,
        flex: 1
    },
    mainContent: {
        flex: 1
    },
    productContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    productName: {
        fontFamily: 'Poppins-Regular',
        fontSize: 16,
        color: 'white'
    },
    desc: {
        fontFamily: 'Poppins-Regular',
        fontSize: 14,
        color: 'white'
    },
    footer: {
        flexDirection: 'row',
        paddingTop: 10,
        alignItems: 'center'
    },
    priceContainer: {
        flex: 1
    },
    button: {
        width: 163
    },
    labelTotal:{
        fontSize: 13,
        fontFamily: 'Poppins-Regular',
        color: 'white'
    },
    priceTotal: {
        fontSize: 18,
        fontFamily: 'Poppins-Regular',
        color: 'white'
    }
})
