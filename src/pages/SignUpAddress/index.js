import React from 'react'
import { ScrollView, StyleSheet, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Gap, Header, Select, TextInput } from '../../components'
import { setLoading, signUpAction } from '../../redux/action'
import { useForm } from '../../utils'

const SignUpAddress = ({navigation}) => {
    const[form, setForm] = useForm({
        address: '',
        houseNumber: '',
        phoneNumber: '',
        city: 'Bandung',
    })

    const dispatch = useDispatch();
    const {registerReducer, photoReducer} = useSelector((state) => state)

    const onSubmit = () => {
        console.log('form: ', form)
        const data = {
            ...form,
            ...registerReducer
        }
        //console.log('data Register: ', data)
        dispatch(setLoading(true))
        dispatch(signUpAction(data, photoReducer, navigation))
    }

    return (
        <ScrollView
            contentContainerStyle = {{ flexGrow:1 }}
            showsVerticalScrollIndicator={false} 
        >
            <View style={styles.page}>
                <Header title='Address' onBack={() => navigation.goBack()} />
                <View style={styles.container}>
                    <Gap height={16} />
                    <TextInput 
                        label='Phone No.' 
                        placeholder='Input phone number'
                        value={form.phoneNumber}
                        onChangeText={(value) => setForm('phoneNumber', value)} 
                    />
                    <Gap height={16} />
                    <TextInput 
                        label='Address' 
                        placeholder='Input address'
                        value={form.address}
                        onChangeText={(value) => setForm('address', value)} 
                    />
                    <Gap height={16} />
                    <TextInput label='House No.' placeholder='Input house number'
                        value={form.houseNumber}
                        onChangeText={(value) => setForm('houseNumber', value)} 
                    />
                    <Gap height={16} />
                    <Select 
                        label="City"
                        value={form.city}
                        onSelectChange={(value) => setForm('city', value)} />
                    <Gap height={24} />
                    <Button 
                        text='Sign Up Now'
                        onPress={onSubmit }
                    />
                </View>
            </View>
        </ScrollView>
    )
}

export default SignUpAddress

const styles = StyleSheet.create({
    page: {
        flex:1
    },
    container:{
        paddingHorizontal: 24,
        paddingTop: 24,
        backgroundColor: 'white',
        flex: 1
    },
    photo: {
        alignItems: 'center'
    },
    borderPhoto: {
        borderWidth: 1, 
        borderColor: '#8D92A3',
        width: 110,
        height: 110,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center'
    },
    photoContainer: {
        width: 90,
        height: 90,
        borderRadius: 90,
        padding: 25,
        backgroundColor: '#F0F0F0',
    },
    addPhoto: {
        fontFamily:'Poppins-Light',
        fontSize: 14,
        textAlign: 'center'
    }
})
