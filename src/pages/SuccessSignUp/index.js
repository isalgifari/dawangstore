import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { IlSuccessSignUp } from '../../assets'
import { Button, Gap } from '../../components'

const SuccessSignUp = ({navigation}) => {
    return (
        <View style={styles.page}>
            <IlSuccessSignUp/>
            <Gap height={30} />
            <Text style={styles.text1}>Yeay!, registrasi berhasil</Text>
            <Text style={styles.text2}>Sekarang anda bisa order</Text>
            <Text style={styles.text2}>apapun yang anda mau</Text>
            <Gap height={30} />
            <View style={styles.buttonContainer}>
                <Button
                    text='Temukan Sesuatu'
                    onPress = {() => navigation.reset({index: 0, routes: [{name: 'MainApp'}]})}
                />
            </View>
        </View>
    )
}

export default SuccessSignUp

const styles = StyleSheet.create({
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    text1: {
        fontFamily: 'Poppins-Regular',
        fontSize: 20,
        color: '#020202'
    },
    text2: {
        fontFamily: 'Poppins-Light',
        fontSize: 14,
        color: '#8D92A3'
    },
    buttonContainer: {
        width: '100%' ,
        paddingHorizontal: 80
    }
})
