import React, {useEffect} from 'react';
import { StyleSheet, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { Button, Gap, Header, TextInput } from '../../components';
import { signIndAction } from '../../redux/action/auth';
import { useForm, getData } from '../../utils';

const SignIn = ({navigation}) => {
    // const [email, setEmail] = useState('');
    // const [password, setPassword] = useState('');
    const [form, setForm] = useForm({
        email: '',
        password: ''
    })

    const dispatch = useDispatch();

    const onSubmit = () => {
        console.log('form: ',form);
        dispatch(signIndAction(form, navigation))
    }

    return (
        <View style={styles.page}>
            <Header title='Sign In' />
            <View style={styles.container}>
                <TextInput 
                    label='Email' 
                    placeholder='Input email'
                    value={form.email}
                    onChangeText={(value) => setForm('email',value)} 
                />
                <Gap height={16} />
                <TextInput 
                    label='Password' 
                    placeholder='Input password' 
                    value={form.password}
                    onChangeText={(value) => setForm('password', value)} 
                    secureTextEntry
                />
                <Gap height={24} />
                <Button 
                    text='Sign In'
                    onPress={onSubmit} 
                />
                <Gap height={12} />
                <Button 
                    text='Create New Account' 
                    color='#2F80ED' 
                    onPress={() => navigation.navigate('SignUp')}
                />
            </View>
        </View>
    )
}

export default SignIn

const styles = StyleSheet.create({
    page: {flex: 1},
    container:{
        paddingHorizontal: 24,
        paddingTop: 24,
        backgroundColor: 'white',
        flex: 1
    }
})
