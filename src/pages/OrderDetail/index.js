import axios from 'axios'
import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { Button, Header, ItemListFood, ItemValue } from '../../components'
import { API_HOST } from '../../config'
import { getData } from '../../utils'

const OrderDetail = ({route, navigation}) => {
    const order  = route.params
    console.log('order detail : ', order)
    const onCancel = () => {
        const data = {
            status: 'CANCELLED'
        }
        getData('token').then((resToken) => {
            axios.post(`${API_HOST.url}/api/transaction/${order.id}`, data, {
                headers: {
                    Authorization: resToken.value
                }
            })
            .then((res) => {
                console.log('success cancel order: ', res)
                navigation.reset({
                    index: 0, 
                    routes: [{name: 'MainApp'}]
                })
            })
            .catch(err => {
                console.log('error cancel order : ', err)
            }) 
        })
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <Header 
                title="Payment Detail"
                onBack={() => navigation.goBack()}
            />
            <View style={styles.container}>
                <View style={styles.content}>
                    <Text style={styles.title}>Item Ordered</Text>
                    <ItemListFood 
                        image={{ uri: order.food.picturePath }}
                        nameProduct={order.food.name}
                        type="order-summary"
                        price={order.food.price}
                        items={order.quantity}
                    />
                </View>
                <View style={styles.content}>
                    <Text style={styles.title}>Detail Transaction</Text>
                    <ItemValue 
                        label={order.food.name}
                        value={order.food.price * order.quantity} 
                        type="currency" 
                    />
                    <ItemValue
                        label="Driver"
                        value={50000}
                        type="currency" 
                    />
                    <ItemValue
                        label="Tax 10%"
                        value={(10 / 100 * order.food.price)} 
                        type="currency" 
                    />
                    <ItemValue
                        label="Total Price"
                        value={order.total} 
                        type="currency" 
                        valueColor="#1ABC9C"
                        
                    />
                </View>
                <View style={styles.content}>
                    <Text style={styles.title}>Deliver to</Text>
                    <ItemValue
                        label="Name"
                        value={order.user.name} 
                    />
                    <ItemValue
                        label="Phone"
                        value={order.user.phoneNumber} 
                    />
                    <ItemValue
                        label="Address"
                        value={order.user.address} 
                    />
                    <ItemValue
                        label="House No"
                        value={order.user.houseNumber} 
                    />
                    <ItemValue
                        label="City"
                        value={order.user.city}
                    />
                </View>
                <View style={styles.content}>
                    <Text style={styles.title}>Order Status</Text>
                    <ItemValue
                        label={`#${order.id}`}
                        value={order.status} 
                        valueColor={order.status === 'CANCELLED' ? "#D9435E" : "#1ABC9C" }
                    />
                </View>
                <View style={styles.button}>
                    {order.status === 'PENDING' && (
                        <Button 
                            text="Batalkan Older Saya"
                            color="#D9435E"
                            onPress={onCancel}
                        />
                    )}
                </View>
            </View>
        </ScrollView>
    )
}

export default OrderDetail

const styles = StyleSheet.create({
    title: {
        color: '#020202',
        fontFamily: 'Poppins-Regular',
        fontSize: 14,
        marginBottom: 5
    },
    content: {
        backgroundColor: 'white',
        marginTop: 10,
        paddingHorizontal: 15,
        paddingVertical: 5
    },
    button: {
        paddingHorizontal: 24,
        paddingVertical: 10
    }
})
